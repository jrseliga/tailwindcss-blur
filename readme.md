# Tailwind CSS blur utilities
Tailwind CSS plugin providing blur utilities.

## Install
`npm i tailwindcss-blur`

## Usage
Register plugin in tailwind configuration (`tailwind.js`).

### With default settings

```
plugins: [
    require('tailwindcss-blur')({}),
],
```

### With custom settings (default values provided as example)

```
plugins: [
    require('tailwindcss-blur')({
        sizes: {
            0: '0',
            2: '2px',
            4: '4px',
            8: '8px',
            16: '16px'
        },
        variants: ['hover']
    }),
],
```